public class PrintNestedRectangle {
	public static void main(String[] args) {
		int h = 0;
		int w = 0; 
		if (args.length != 2) {
			System.out.println("Try to enter two ints");
		} else {
			try{
				h = Integer.parseInt(args[0]);
				w = Integer.parseInt(args[1]);
			} catch(Exception e) {
				System.out.println("Inputs must be int");
				System.exit(0);
			}
			if (h <= 0 || w <= 0) {
				System.out.println("Inputs must be positive numbers");
			} else {

				System.out.println("Height: "+ h + " Width: "+ w);
				
				char[][] m = new char[h][w];

				for (int i=0; i<h; i++){
					for (int j=0; j<w; j++){
						if (i == 0 || j == 0 ){
							m[i][j] = '#';
						}
						else if (i == h-1 || j == w-1){
							m[i][j] = '#';
						}
						else if (i > 1 && i < h-2 && j > 1 && j < w-2) {
							m[i][j] = '#';
						}
						if(i > 2 && i < h-3 && j > 2 && j < w-3){
							m[i][j] = ' ';
						}
					}
				}
				for (int i = 0; i < m.length; i++){
					System.out.println(m[i]);
				}
			}
			
		}
	}
}